package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

import java.beans.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ConsultationListImpl extends java.lang.Object implements Serializable, ConsultationList, VetoableChangeListener {

    private PropertyChangeSupport propertyChangeSupport;
    private final VetoableChangeSupport vetoableChangeSupport = new VetoableChangeSupport(this);

    public List<Consultation> consultations;
    public int size;

    public ConsultationListImpl() {
        propertyChangeSupport = new PropertyChangeSupport(this);
        consultations = new LinkedList<Consultation>();
    }

    public ConsultationListImpl(List<Consultation> consultations) {
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.consultations = consultations;
    }

    public ConsultationListImpl(LinkedList<Consultation> consultations){
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.consultations = new LinkedList<Consultation>();
        try{
            for (Consultation consultation: consultations){
                this.addConsultation(consultation);
            }
        } catch (PropertyVetoException ex){

        }
    }

    @Override
    public int getSize() {
        return this.consultations.size();
    }

    @Override
    public Consultation[] getConsultation() {
        return (consultations.toArray(new Consultation[consultations.size()]));
    }
    public void setConsultation(List consultations) {
        this.consultations = consultations;
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.getConsultation()[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        try{
            int oldSize = this.getSize();
            if (oldSize > 0) {
                vetoableChangeSupport.addVetoableChangeListener((VetoableChangeListener)consultation);
                vetoableChangeSupport.fireVetoableChange("consultation", consultations.get(getSize() - 1), consultation);
            }

            ((ConsultationImpl)consultation).addVetoableChangeListener(this);
            consultations.add(consultation);

            propertyChangeSupport.firePropertyChange("consultation", oldSize, oldSize + 1);

        } catch(PropertyVetoException e) {
            throw e;
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.removeVetoableChangeListener(listener);
    }

    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException {
        ConsultationImpl oldConsultation = (ConsultationImpl)event.getOldValue();

        int minutes = (Integer)event.getNewValue();

        long consultationStart = oldConsultation.getTerm().getBegin().getTime();

        long endAfterProlong = oldConsultation.getTerm().getEnd().getTime() + minutes * 60000;

        for (Consultation consultation : consultations) {
            long start = ((ConsultationImpl)consultation).getTerm().getBegin().getTime();
            if((consultationStart < start) && (endAfterProlong >= start)) {
                throw new PropertyVetoException("duration", event);
            }
        }
    }
}
