package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = TermImpl.class;
    public static Class<? extends Consultation> consultationBean = ConsultationImpl.class;
    public static Class<? extends ConsultationList> consultationListBean = ConsultationListImpl.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ConsultationListFactoryImpl.class;
    
}
