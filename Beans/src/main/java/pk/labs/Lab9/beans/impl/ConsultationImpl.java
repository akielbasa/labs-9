package pk.labs.Lab9.beans.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

import java.io.Serializable;
import java.util.Date;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;

public class ConsultationImpl extends Object implements Serializable, Consultation, VetoableChangeListener {

    public String student;
    public PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    private Term term;
    private final VetoableChangeSupport vetoableChangeSupport = new VetoableChangeSupport(this);

    public ConsultationImpl() {
        student = new String("");
        propertyChangeSupport = new PropertyChangeSupport(this);
        term = new TermImpl();
    }

    public ConsultationImpl(String student, TermImpl term) {
        this.student = student;
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.term = term;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Term getTerm() {
        return term;
    }

    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        this.term = term;
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        if(minutes > 0) {
            try {
                vetoableChangeSupport.fireVetoableChange("duration", this, minutes);
                this.term.setDuration(this.term.getDuration() + minutes);
            }
            catch(PropertyVetoException e) {
                throw e;
            }
        }
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.removeVetoableChangeListener(listener);
    }

    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException {
        ConsultationImpl oldConsultation = (ConsultationImpl)event.getOldValue();
        ConsultationImpl newConsultation = (ConsultationImpl)event.getNewValue();

        long oldConsultationBegin = oldConsultation.term.getBegin().getTime();
        long oldConsultationEnd = oldConsultation.term.getEnd().getTime();

        long newConsultationBegin = newConsultation.term.getBegin().getTime();
        long newConsultationEnd = newConsultation.term.getEnd().getTime();

        if(oldConsultationBegin <= newConsultationBegin && oldConsultationEnd >= newConsultationBegin) {
            throw new PropertyVetoException("consultation", event);
        } else
        if(oldConsultationBegin >= newConsultationBegin && newConsultationEnd >= oldConsultationBegin) {
            throw new PropertyVetoException("consultation", event);
        }
    }
}

